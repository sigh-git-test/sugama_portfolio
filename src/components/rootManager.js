import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import TopPage from './topPage';



function RootManager(props){
  return(
		<div>
      <BrowserRouter>
        <div>
          <Route path="/" exact component={TopPage} />
          <Route path="/preview"  component={PageTwo} />
        </div>
      </BrowserRouter>
    </div>
	)
}

export default RootManager;